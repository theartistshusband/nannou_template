use std::process;

use nannou::prelude::*;

const WIDTH: u32 = 600;
const HEIGHT: u32 = 600;
const CAPTURE: bool = false;

fn main() {
    nannou::app(model)
        .update(update)
        .run();
}

struct Model {
    point: Vec2,

    rect: Rect,
    draw: Draw,
    paused: bool,
    ctrl_key_pressed: bool,
}

fn model(app: &App) -> Model {
    app
        .new_window()
        .size(WIDTH, HEIGHT)
        .view(view)
        .key_pressed(key_pressed)
        .key_released(key_released)
        .build()
        .unwrap();

    let rect = app.window_rect();

    Model {
        point: vec2(0., 0.),

        rect,
        draw: app.draw(),
        paused: false,
        ctrl_key_pressed: false,
    }
}

fn update(app: &App, model: &mut Model, _update: Update) {
    if model.paused { return; }

    let rect = app.window_rect();
    model.point = vec2(
        map_range(random_f32(), 0., 1., rect.left(), rect.right()),
        map_range(random_f32(), 0., 1., rect.bottom(), rect.top()),
    )
}

fn view(app: &App, model: &Model, frame: Frame) {
    if model.paused { return; }

    if app.elapsed_frames() == 1 {
        model.draw.background().color(WHITE);
    }
    let color = random_f32();
    let size = map_range(random_f32(), 0., 1., 3., 10.);

    model.draw.ellipse()
        .hsla(color, 1., 0.5, 0.5)
        .w(size)
        .h(size)
        .xy(model.point);

    model.draw.to_frame(app, &frame).unwrap();

    if CAPTURE {
        let file_path = captured_frame_path(app, &frame);
        app.main_window().capture_frame(file_path);
    }
}

/// React to key-presses
fn key_pressed(app: &App, model: &mut Model, key: Key) {
    match key {
        Key::C => {
            if model.ctrl_key_pressed {
                process::exit(0);
            }
        }
        Key::S => {
            let file_path = saved_image_path(app);
            app.main_window().capture_frame(file_path);
        }
        Key::Space => {
            model.paused = !model.paused;
        }
        Key::LControl => {
            model.ctrl_key_pressed = true;
        }
        _other_key => {}
    }
}

/// React to key releases
fn key_released(_app: &App, model: &mut Model, key: Key) {
    match key {
        Key::LControl => {
            model.ctrl_key_pressed = false;
        }
        _other_key => {}
    }
}

/// Get the path to the next captured frame
fn captured_frame_path(app: &App, frame: &Frame) -> std::path::PathBuf {
    app.project_path()
        .expect("failed to locate `project_path`")
        .join("frames")
        .join(format!("frame{:05}", frame.nth()))
        .with_extension("png")
}

/// Get the path to the next saved image
fn saved_image_path(app: &App) -> std::path::PathBuf {
    app.project_path()
        .expect("failed to locate `project_path`")
        .join("saved")
        .join(format!("image{:05}", chrono::offset::Local::now()))
        .with_extension("png")
}
